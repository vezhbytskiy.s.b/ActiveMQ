package shpp.app.validation;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class EEDRValidator {

    private String errorMessage;
    private int SIZE_EDDR = 13;
    //
    private int[] REPETITIVE_WEIGHT = {7, 3, 1};

    public boolean isValid(Object o) throws IllegalAccessException {
        Class clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(EDDRValidation.class)) {
                EDDRValidation annotation = field.getAnnotation(EDDRValidation.class);
                field.setAccessible(true);
                String str = (String) field.get(o);

                return str != null && str.length() == SIZE_EDDR && validationDate(str)
                        && validationControlNumber(str);
            }
        }

        if (errorMessage == null) {
            errorMessage = "eddr is incorrect";
        }

        return false;
    }

    private boolean validationDate(String validatedStr) {
        String dateString = validatedStr.substring(0, 8);  // YYYYMMDD format

        // Define a formatter
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        try {
            // Parse the string into a LocalDate object
            LocalDate date = LocalDate.parse(dateString, formatter);
        } catch (DateTimeParseException e) {
            errorMessage = "Invalid date format: " + dateString;

            errorMessage = "Error: incorrect date in eddr";
            return false;
        }

        return true;
    }

    /**
     * A special check digit calculation has been adopted for use in MRTDs.
     * The check digits shall be calculated on modulus
     * 10 with a continuously repetitive weighting of 731 731 ..., as follows.
     *
     * @param validateStr string that we validate
     * @return true if the control number is correct and false otherwise
     */
    private boolean validationControlNumber(String validateStr) {
        char[] numbersOfEDDR = validateStr.toCharArray();

        int sum = 0;
        for (int i = 0; i < numbersOfEDDR.length - 1; i++) {
            sum += Character.getNumericValue(numbersOfEDDR[i])
                    * REPETITIVE_WEIGHT[i % REPETITIVE_WEIGHT.length];
        }

        if (!(Character.getNumericValue(numbersOfEDDR[numbersOfEDDR.length - 1]) == sum % 10)) {
            errorMessage = "Error: incorrect control number in eddr";
            return false;

        }
        return true;
    }


    public String getErrorMessage() {
        return errorMessage;
    }
}
