package shpp.app;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.app.jms.Consumer;
import shpp.app.jms.JMSQueue;
import shpp.app.jms.Producer;

import javax.jms.JMSException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class App {
    private static final String PROPERTY_FILE_NAME = "app.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final String POISON_PILL_MESSAGE = "THE END OF THE QUEUE";
    private static final String CORRECT_DATA_FILE = "csvWithCorrectData.csv";
    private static final String INCORRECT_DATA_FILE = "csvWithIncorrectData.csv";

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        ConfigManager configManager = new ConfigManager();

        configManager.readConfiguration(PROPERTY_FILE_NAME);

        JMSQueue queue = new JMSQueue();

        try {
            queue.init(configManager.getQueueName(), configManager.getBrokerURL(),
                    configManager.getUserName(), configManager.getUserPassword());

            Producer producer = queue.createProducer();

            long beforeGeneration = System.currentTimeMillis();

            //generate messages
            LOGGER.info("start generation messages");
            generateMessages(configManager, producer);
            LOGGER.info("Generation time {}", System.currentTimeMillis() - beforeGeneration);

            beforeGeneration = System.currentTimeMillis();

            //read messages
            LOGGER.info("start reading and validation messages");
            Consumer consumer = queue.createConsumer();
            MessageHandler messageHandler = new MessageHandler();
            messageHandler.readMessages(consumer, POISON_PILL_MESSAGE);
            LOGGER.info("Reading and validation time {}", System.currentTimeMillis() - beforeGeneration);


            //write information about pojo to files
            LOGGER.info("Start writing messages into csv files");
            writeMessagesToCSV(messageHandler);

            consumer.close();
            producer.close();
            queue.close();
        } catch (Exception e) {
            LOGGER.error(e.toString(), e);
            try {
                LOGGER.error("Attempting to roll back the transaction...");
                queue.rollback();
                LOGGER.info("Rollback successful.");
            } catch (JMSException ex) {
                LOGGER.error("Rollback failed: " + ex.getMessage(), ex);
            }
        }
        long endTime = System.currentTimeMillis();
        LOGGER.info("{}",endTime - startTime);
    }

    private static void writeMessagesToCSV(MessageHandler messageHandler) throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException {
        try (FileWriter writerCorrect = new FileWriter(CORRECT_DATA_FILE, true);
             CSVWriter writerIncorrect = new CSVWriter(new FileWriter(INCORRECT_DATA_FILE, true))) {

            StatefulBeanToCsv<POJO> statefulBeanToCsv = new StatefulBeanToCsvBuilder<POJO>(writerCorrect).build();
            statefulBeanToCsv.write(messageHandler.getListWithCorrectPojo());

            writeIncorrectData(messageHandler.getListWithIncorrectPojo(), messageHandler.getListWithErrors(), writerIncorrect);

        } catch (Exception e) {
            LOGGER.error("Error writing to CSV files: ", e);
            throw e;
        }
    }

    private static void writeIncorrectData(List<POJO> incorrectDataList, List<String> errorList, CSVWriter fileWriter) throws IOException {
        try {
            for (int i = 0; i < incorrectDataList.size() && i < errorList.size(); i++) {
                String[] tempRow = {
                        incorrectDataList.get(i).getName(),
                        String.valueOf(incorrectDataList.get(i).getCount()),
                        errorList.get(i)
                };
                fileWriter.writeNext(tempRow);
            }
        } catch (Exception e) {
            LOGGER.error("Error writing incorrect data: ", e);
            throw e;
        }
    }

    private static void generateMessages(ConfigManager configManager, Producer producer) {
        MessageGenerator messageGenerator = new MessageGenerator();
        messageGenerator.generateMessages(configManager.getNumberSentMessages(), producer,
                configManager.getPoisonPill(), POISON_PILL_MESSAGE);
    }

}
