package shpp.app;

import java.util.Properties;

public class ConfigManager {
    private int numberSentMessages = 1000;
    private String queueName;
    private String poisonPillMessage = "THE END OF THE QUEUE";
    private String brokerURL;
    private long poisonPill;
    private String userName;
    private String userPassword;

    public void readConfiguration(String propertyFileName) {
        if (System.getProperty("N") != null) {
            numberSentMessages = Integer.parseInt(System.getProperty("N"));
        }
        numberSentMessages = 1000;
        PropertyReader propertyReader = new PropertyReader();
        Properties properties = propertyReader.readProperties(propertyFileName);
        if (properties != null && !properties.isEmpty()) {
            queueName = properties.getProperty("queueName");
            brokerURL = properties.getProperty("brokerURL");
            poisonPill = Long.valueOf(properties.getProperty("poisonPill"));
            userName = properties.getProperty("userName");
            userPassword = properties.getProperty("userPassword");
        }
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public int getNumberSentMessages() {
        return numberSentMessages;
    }

    public String getQueueName() {
        return queueName;
    }

    public String getPoisonPillMessage() {
        return poisonPillMessage;
    }

    public String getBrokerURL() {
        return brokerURL;
    }

    public long getPoisonPill() {
        return poisonPill;
    }

}
