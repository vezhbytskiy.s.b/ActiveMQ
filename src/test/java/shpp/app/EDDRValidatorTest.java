package shpp.app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import shpp.app.validation.EEDRValidator;

public class EDDRValidatorTest {
    private EEDRValidator eedrValidator;

    @Before
    public void setUp() {
        eedrValidator = new EEDRValidator();
    }

    @Test
    public void testWithCorrectEDDRs() throws IllegalAccessException {
        String[] correctEddr = {"1971030380592",
                "1918032990679",
                "1969031368060",
                "2022052144783",
                "1984101217383",
                "1963030625629",
                "1922020992655",
                "1974010381048",
                "1921062768349",
                "1902102070622",
                "2021041570235",
                "1990020829299",
                "1934042915132",
                "1916012662240",
                "1918082175189",
                "2018020692107",
                "1946040626947",
                "1907082646249",
                "2004092317875",
                "1971052951589",
                "1901092375717",
                "1969071278420",
                "1972071452918"};

        POJO pojo = new POJO();
        for (String currentEddr : correctEddr
        ) {
            pojo.setEDDR(currentEddr);
            Assert.assertTrue(eedrValidator.isValid(pojo));
        }

    }

    @Test
    public void testWithIncorrectEDDRs() throws IllegalAccessException {
        String[] correctEddr = {"1971030380512",
                "1918032990629",
                "1969031368020",
                "2022052144723",
                "1984101217323",
                "1963030625619",
                "1922020992625",
                "1974010381018",
                "1921062768329",
                "1902102070612",
                "2021041570225",
                "1990020829229",
                "1934042915122",
                "1916012662220",
                "1918082175129",
                "2018020692127",
                "1946040626927",
                "1907082646229",
                "2004092317825",
                "1971052951519",
                "1901092375727",
                "1969071278410",
                "1972071452928"};

        POJO pojo = new POJO();
        for (String currentEddr : correctEddr
        ) {
            pojo.setEDDR(currentEddr);
            Assert.assertFalse(eedrValidator.isValid(pojo));
        }
    }

}
